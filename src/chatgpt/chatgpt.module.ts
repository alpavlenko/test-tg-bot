import { Module } from '@nestjs/common';

import { ChatGptService } from './chatgpt.service';

// prettier-ignore
@Module({
  providers: [ChatGptService],
  exports: [ChatGptService]
})
export class ChatGptModule {}
