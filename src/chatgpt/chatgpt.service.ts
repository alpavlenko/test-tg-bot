import axios from "axios";

export class ChatGptService {
  private readonly apiUrl = "https://api.openai.com/v1/chat/completions";

  public async makeRequest(
    question: string
  ): Promise<string> {
    const model = "gpt-4o-2024-05-13";
    const apiKey = process.env.GPT_API_KEY;

    const messages = [{
      role: "user",
      content: question
    }];

    try {
      const { data } = await axios.post(
        this.apiUrl, { model, messages }, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${apiKey}`
          }
        }
      );
      return data?.choices?.[0]?.message?.content;
    } catch (error: unknown) {
      console.error(error, 'makeRequest')
    }
  }
}