import { Request, Response } from "express";
import { Controller, Post, Req, Res } from "@nestjs/common";

import { TelegramService } from "./telegram.service";
import { ITelegramResponse } from "./telegram.interface";


@Controller()
export class TelegramController {
  constructor(private readonly telegramService: TelegramService) {}

  @Post('telegram-webhook')
  async webhook(@Req() req: Request, @Res() res: Response) {
    try {
      res.status(200).send();
      const { body } = req as unknown as ITelegramResponse;
      return await this.telegramService.handleMessage(
        body.message
      );
    } catch (e) {
      console.log(e);
    }
  }
}
