export interface ITelegramUser {
  id: number;
  username: string;
  first_name: string;
  language_code: string;
  is_bot: boolean;
  is_premium?: boolean;
}

export interface ITelegramChat {
  id: number;
  type: string;
  title?: string;
  username?: string;
  first_name?: string;
}

export interface ITelegramPhoto {
  file_id: string;
  file_size: number;
  file_unique_id: string;
  width: number;
  height: number;
}

export interface ITelegramVoice {
  file_id: string;
  file_size: number;
  file_unique_id: string;
}

export interface ITelegramEntity {
  type: string;
  offset: number;
  length: number;
}

export interface ITelegramForward {
  type: string;
  date: number;
  sender_user: ITelegramUser;
}

export interface ITelegramMessage {
  message_id: number;
  from: ITelegramUser;
  chat: ITelegramChat;
  date: number;
  text?: string;
  caption?: string;
  edit_date?: number;
  voice: ITelegramVoice;
  photo?: ITelegramPhoto[];
  entities?: ITelegramEntity[];
  media_group_id?: number;
  forward_date?: number;
  forward_from?: ITelegramUser;
  forward_origin?: ITelegramForward;
  reply_to_message?: ITelegramMessage;
  business_connection_id?: string;
}


export interface ITelegramBody {
  update_id: number;
  message?: ITelegramMessage;
}

export interface ITelegramQuery {
  user_id: string;
  channel_id: string;
}

export interface ITelegramResponse {
  body: ITelegramBody;
  query: ITelegramQuery;
}
