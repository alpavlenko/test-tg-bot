import { Module } from '@nestjs/common';

import { TelegramService } from './telegram.service';
import { TelegramController } from './telegram.controller';

import { ChatGptModule } from '../chatgpt/chatgpt.module';

// prettier-ignore
@Module({
  imports: [
    ChatGptModule
  ],
  providers: [TelegramService],
  controllers: [TelegramController],
})
export class TelegramModule {}
