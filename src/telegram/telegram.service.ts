import axios from "axios";
import * as speech from "@google-cloud/speech";

import { HttpException, Injectable, OnModuleDestroy, OnModuleInit } from "@nestjs/common";
import { ITelegramMessage, ITelegramVoice } from "./telegram.interface";
import { ChatGptService } from "../chatgpt/chatgpt.service";

enum ReceiveMethod {
  UPDATES = "UPDATES",
  WEBHOOK = "WEBHOOK"
}

@Injectable()
export class TelegramService implements OnModuleInit, OnModuleDestroy {
  private readonly apiUrl = "https://api.telegram.org";

  private lastConfirmedUpdateId: number;
  private speechClient: speech.SpeechClient;

  constructor(
    private readonly gptService: ChatGptService,
  ) {
    this.speechClient = new speech.SpeechClient({
      keyFilename: "credential.json"
    });
  }

  public async onModuleInit() {
    console.log("onModuleInit");
    if (process.env.BOT_TOKEN) {
      const method = process.env.RECEIVE_METHOD;
      if (method == ReceiveMethod.UPDATES) {
        setTimeout(() => this.handleUpdates(), 1000);
      } else {
        await this.connectWebhook();
      }
    }
  }

  private async sendMessage(
    message: ITelegramMessage,
    text: string
  ): Promise<boolean> {
    try {
      const token = process.env.BOT_TOKEN;
      const { status } = await axios.post(
        `${this.apiUrl}/bot${token}/sendMessage`,
        {
          text: text,
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id
        }
      );
      return status === 200;
    } catch (error: unknown) {
      console.error(error, "sendMessage");
    }
  }

  // prettier-ignore
  private async loadFile(
    id: string
  ): Promise<ArrayBuffer> {
    const token = process.env.BOT_TOKEN;
    const { data: { result } } = await axios.get(
      `${this.apiUrl}/bot${token}/getFile?file_id=${id}`
    );
    if (!result?.file_path) throw new HttpException(
      `Failed to retrieve file path for id: ${id}`, 500
    );
    const { data: fileData } = await axios.get(
      `${this.apiUrl}/file/bot${token}/${result.file_path}`,
      { responseType: "arraybuffer" }
    );
    return fileData;
  }

  // prettier-ignore
  private async processVoice(
    voice: ITelegramVoice
  ): Promise<string | undefined> {
    const fileContent = await this.loadFile(
      voice.file_id
    );
    try {
      const buffer = Buffer.from(fileContent);
      const [response] = await this.speechClient.recognize({
        audio: { content: buffer.toString("base64") },
        config: {
          encoding: "OGG_OPUS",
          languageCode: "ru-RU",
          sampleRateHertz: 48000
        }
      });
      if (response.results.length > 0) {
        const result = response.results[0];
        return result.alternatives?.[0]?.transcript;
      }
    } catch (error: unknown) {
      console.error(error, "processVoice");
    }
  }

  public async handleMessage(
    message: ITelegramMessage
  ): Promise<boolean> {
    if (message.text === "/start") {
      const text = `Hi, ${message.from.username}`;
      return await this.sendMessage(message, text);
    } else if (message?.voice) {
      const transcript = await this.processVoice(
        message?.voice
      );
      if (transcript) {
        let text = await this.gptService.makeRequest(
          transcript
        );
        if (!text) text = 'GPT request error';
        return await this.sendMessage(message, text);
      }
      const text = `Voice recognition error`;
      return await this.sendMessage(message, text);
    } else if (message.text) {
      let text = await this.gptService.makeRequest(
        message.text
      );
      if (!text) text = 'GPT request error';
      return await this.sendMessage(message, text);
    }
    return false;
  }

  private async connectWebhook(): Promise<void> {
    const url = process.env.WEBHOOK_URL;
    const token = process.env.BOT_TOKEN;
    try {
      const { status } = await axios.get(
        `${this.apiUrl}/bot${token}/setWebhook`
        + `?url=${encodeURIComponent(url)}`
      );
      if (status === 200) {
        console.log("Webhook connected");
      }
    } catch (error: unknown) {
      console.error(error, "onModuleInit");
    }
  }

  private async handleUpdates(): Promise<void> {
    const token = process.env.BOT_TOKEN;
    const lastId = this.lastConfirmedUpdateId;
    const { data } = await axios.get(
      `${this.apiUrl}/bot${token}/getUpdates`
      + (lastId ? `?offset=${lastId + 1}` : "")
    );

    for (const body of data.result) {
      await this.handleMessage(body.message);
      this.lastConfirmedUpdateId = body.update_id;
    }
    setTimeout(() => this.handleUpdates(), 1000);
  }

  public async onModuleDestroy(): Promise<void> {
    console.log("onModuleDestroy");
    const method = process.env.RECEIVE_METHOD;
    if (method !== ReceiveMethod.UPDATES) {
      const token = process.env.BOT_TOKEN;
      try {
        const { status } = await axios.get(
          `${this.apiUrl}/bot${token}/deleteWebhook`
        );
        if (status === 200) {
          console.log("Webhook disconnected");
        }
      } catch (error: unknown) {
        console.error(error, "onModuleDestroy");
      }
    }
  }
}